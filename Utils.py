import requests
import json

def json_from_http(url: str):
    try:
        data = requests.get(url)
        data = json.loads(data.text)
        return data
    except Exception as e:
        print("Error: " + str(e))
        return None
