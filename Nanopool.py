from Utils import json_from_http
BASE_URL_BALANCE = "https://api.nanopool.org/v1/{}/balance/{}"


def get_balance(coin: str, wallet: str) -> float:
    pool_data = json_from_http(BASE_URL_BALANCE.format(coin, wallet))
    if pool_data is not None:
        return float(pool_data['data'])
    else:
        return 0.0


def crypto_to_usd(coin: str, amount: float):
    url = "https://min-api.cryptocompare.com/data/price?fsym={}&tsyms=BTC,USD,EUR".format(coin.upper())
    data = json_from_http(url)
    try:
        if data is not None:
            return data["USD"]
        else:
            return 0
    except Exception as e:
        print("Error: " + str(e))

