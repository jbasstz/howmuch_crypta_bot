import time

from Nanopool import get_balance, crypto_to_usd

cows = [
    {
        "coins":
            [
                {
                    "coin": "eth",
                    "wallet": "0x97d4a00bAFa1d17258FE83e05bf6fb1720fb7a05"
                }
            ],

        "name": "Утка"
    },
    {
        "coins":
            [
                {
                    "coin": "eth",
                    "wallet": "0xf3424112f14ce11bb48b9b1e2d57230a379ec447"
                }
            ],

        "name": "ККГ"
    },
    {
        "coins":
            [
                {
                    "coin": "eth",
                    "wallet": "0x942c457340c9b2A16227902d654D9f81F1c94B8A"
                }
            ],

        "name": "Какой-то поц"
    }
]


try:
    while True:
        for cow in cows:
            for coin in cow['coins']:
                balance = get_balance(coin['coin'], coin['wallet'])
                if balance is not None:
                    to_usd = crypto_to_usd(coin['coin'], balance)
                    print("{} нафармил {:.5f} ETH или {:.2f} USD"
                          .format(cow['name'], balance, balance * float(to_usd)))
        time.sleep(10 * 60)  # в секундах
except KeyboardInterrupt:
    pass
